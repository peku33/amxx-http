#if defined _http_included
  #endinput
#endif
#define _http_included

#pragma semicolon 1

#include <amxmodx>
#include <sockets>

new const http_task_id = 577;
new const Float:http_task_interval = 0.1;
new const Float:http_task_timeout = 5.0;
new const http_global_buffer_hwm = 128; // amxx czasami dodaje kilka bajt�w rezerwy pomi�dzy rozmiarem bufora a tym co zrobi recv. 

#define HTTP_CALL_ON_FINISH_MAX_LENGTH 128
new http_call_on_finish[HTTP_CALL_ON_FINISH_MAX_LENGTH];
new http_socket = 0;

#define HTTP_BUFFER_SIZE 2048
new http_global_buffer[HTTP_BUFFER_SIZE + 1];
new http_global_buffer_position;
new http_header_processed; // Czy nag��wek zosta� ju� pomini�ty?
new Float:http_timeout_time; // Czas w jakim po��czenie zostanie uznane za zerwane

enum http_error_reason { IN_PROGRESS, CONNECTION_FAILURE, TIMEOUT, MISSING_HEADER, MALFORMED_HEADER };
enum http_progress_reason { BUFFER_FULL, TRANSACTION_END };


/**
	W danej chwili mo�liwa jest tylko jedna transakcja http.
	Kolejn� transakcj� mo�na rozpocz�� po wykonaniu http_kill() albo w funkcji {call_on_finish}_progress, je�li reason == TRANSACTION_END
	
	(bool) http_transaction_in_progress() - czy istnieje zapytanie http "w trakcie"?
	
	(void) http_begin(const url[], const call_on_finish[]) - g��wna funkcja, kt�ra powinna by� wykorzystywana przez u�ytkownika. Rozpoczyna transakcj� HTTP:
		url (string) - adres w postaci "http:// ... "
		call_on_finish (string) prefix nazw funkcji callback�w (patrz ni�ej)
	
		U�ytkownik uruchamiaj�cy powinien przygotowa� dwie funkcje:
			- {call_on_finish}_error - kiedy transakcja jest ko�czona b��dem. Przekazywany jest jeden parametr typu http_error_reason oznaczaj�cy numer b��du
			- {call_on_finish}_progress - kiedy wymagana jest interakcja u�ytkownika z buforem - koniec transakcji z sukcesem albo nape�nienie si� bufora.
			
			Opis w/w funkcji:
				public {call_on_finish}_error(http_error_reason:reason)
				
					reason - zawiera informacj� o b��dzie
					
					Po tej funkcji po��czenie jest automatycznie zamykane i mo�na rozpocz�� nowe
				
			Do skopiowania:
			
			public {call_on_finish}_progress(http_progress_reason:reason, const data[], data_length)
			public {call_on_finish}_error(http_error_reason:reason)
			
	(void) http_kill()
*/

stock http_transaction_in_progress()
{
	return http_socket > 0;
}
stock http_kill()
{
	http_connection_close_finalize();
}
stock http_begin(const url[], const call_on_finish[])
{
	if(http_transaction_in_progress())
	{
		set_fail_state("http_transaction_in_progress()");
		return;
	}
	
	// Zapisujemy do schowka nazw� callbacka do zawo�ania na zako�czenie transakcji
	http_socket = 0;
	
	if(strlen(call_on_finish) > charsmax(http_call_on_finish))
	{
		set_fail_state("strlen(call_on_finish) > charsmax(http_call_on_finish)");
		return;
	}
	
	copy(http_call_on_finish, charsmax(http_call_on_finish), call_on_finish);
	
	// Wyizolowujemy nazw� domeny i URL
	new const http_prefix[] = "http:// ";
	new const http_prefix_length = strlen(http_prefix);
	
	if(contain(url, http_prefix) != 0)
	{
		set_fail_state("[HTTP] http_prefix mismatch #1");
		return;
	}
	
	new domain_end_pos = contain(url[http_prefix_length], "/");
	if(domain_end_pos <= 0)
	{
		set_fail_state("[HTTP] http_prefix mismatch #2");
		return;
	}
	
	new domain[64];
	copy(domain, domain_end_pos, url[http_prefix_length]);
	
	domain_end_pos += http_prefix_length;
	
	// Otwieramy po��czenie do docelowego serwera WWW
	new http_socket_error;
	http_socket = socket_open(domain, 80, SOCKET_TCP, http_socket_error);
	if(http_socket <= 0)
	{
		log_amx("[HTTP] http_begin() socket_open() failed (%d, %d)", http_socket, http_socket_error);
		http_on_failure_close_finalize(CONNECTION_FAILURE);
		return;
	}
	
	// Nadajemy ��danie.
	new get_length = formatex(http_global_buffer, charsmax(http_global_buffer), "GET %s HTTP/1.0^r^nHost: %s^r^nConnection: close^r^n^r^n", url[domain_end_pos], domain);
	socket_send2(http_socket, http_global_buffer, get_length);
	
	// Przygotowujemy odbi�r
	http_global_buffer_position = 0;
	http_header_processed = false;
	http_timeout_time = get_gametime() + http_task_timeout;
	
	http_restart_receiver_task();
}

stock http_connection_close_finalize()
{
	http_stop_receiver_task();
	socket_close(http_socket);
	http_socket = 0;
}
stock http_on_failure_close_finalize(http_error_reason:reason)
{
	http_connection_close_finalize();
	http_on_failure_call(reason);
}
stock http_on_failure_call(http_error_reason:reason)
{
	// Zawo�aj funkcj� u�ytkownika odpowiedzialn� za obs�ug� b��du
	new call[128];
	formatex(call, charsmax(call), "%s_error", http_call_on_finish);
	
	callfunc_begin(call);
	callfunc_push_int(reason);
	callfunc_end();
}

stock http_restart_receiver_task()
{
	http_stop_receiver_task();
	set_task(http_task_interval, "http_receiver_task_", http_task_id, _, 0, "b");
}
stock http_stop_receiver_task()
{
	if(task_exists(http_task_id))
		remove_task(http_task_id);
}
public http_receiver_task_()
{
	http_receiver_task();
}
stock http_receiver_task()
{
	if(http_socket <= 0)
	{
		http_stop_receiver_task();
		return;
	}
	
	while(socket_change(http_socket, 1) > 0)
		http_receiver_receive();
		
	if(get_gametime() > http_timeout_time)
	{
		http_on_failure_close_finalize(TIMEOUT);
		return;
	}
}

stock http_receiver_receive()
{
	// Czy miejsce w lokalnym buforze si� sko�czy�o i nale�y go zwolni�?
	new must_free_buffer = false;
	// Czy otrzymali�my koniec po��czenia i nale�y powiadomi� u�ytkownika?
	new is_message_end = false;
	
	new received_data_length = socket_recv(http_socket, http_global_buffer[http_global_buffer_position], charsmax(http_global_buffer) - http_global_buffer_position); // dodajemy 1
	
	// Po��czenie zosta�o zamkni�te - ko�czymy transakcj�
	if(received_data_length <= 0)
		is_message_end = true;
		
	// Naprawa bufora
	for(new i = http_global_buffer_position; i < http_global_buffer_position + received_data_length; ++i)
		http_global_buffer[i] &= 0xFF;
		
	// Przesuwamy wska�nik zapisu
	http_global_buffer_position += received_data_length;
	
	// Zerujemy ostatni znak aby zapobiec buffer_overflow
	http_global_buffer[http_global_buffer_position] = 0;
	
	// Je�li bufor jest pe�ny - konieczne jest jego zwolnienie przez funkcj� u�ytkownika
	// Istnieje szansa, �e AMX nigdy nie nape�ni bufora do pe�na. Dlatego stosujemy HWM, �eby zapobiec sytuacji w kt�rej b�dziemy podawa� za ma�y bufor aby wpisa� nawet 1 znak
	if(http_global_buffer_position >= (charsmax(http_global_buffer) - http_global_buffer_hwm))
		must_free_buffer = true;
	
	new http_progress_reason:reason = http_progress_reason:-1;
	if(must_free_buffer)
		reason = BUFFER_FULL;
	if(is_message_end)
		reason = TRANSACTION_END;
	
	// Przetwarzamy przychodz�ce dane
	// Je�i reason == -1 - oczekujemy na dalsze dane
	
	// W to miejsce wchodzimy tylko i wy��cznie je�li bufor si� napelni� lub po��czenie zosta�o zamkni�te.
	// Nag��wek musi by� ju� obecny
	new http_header_position = 0;
	if(reason != (http_progress_reason:-1) && !http_header_processed)
	{
		if(http_global_buffer_position < 4)
		{
			// Wiadomos� ma mniej ni� 4 znaki, wi�c na pewno nie zawiera nag��wka
			http_on_failure_close_finalize(MISSING_HEADER);
			return;
		}
			
		// Znajd� miejsce wyst�pienia nag��wka HTTP
		// Uwaga na fakt, �e bufor niekoniecznie ko�czy si� \0 w kt�rymkolwiek miejscu
		for(new i = 0; i < http_global_buffer_position - 4; i++)
		{
			if(
				http_global_buffer[i + 0] == '^r' &&
				http_global_buffer[i + 1] == '^n' &&
				http_global_buffer[i + 2] == '^r' &&
				http_global_buffer[i + 3] == '^n'
			)
			{
				http_header_position = i + 4;
				break;
			}
		}
		
		if(http_header_position == 0)
		{
			// Nag��wek nie zosta� znaleziony.
			// Po drugiej stronie nie ma serwera / albo nie wiadomo co
			http_on_failure_close_finalize(MISSING_HEADER);
			return;
		}
		
		http_header_processed = true;
		
		// Nag��wek jest, sprawdzamy status HTTP
		// Zamieniamy pierwszy znak za nag��wkiem na NULL, coby nie szuka� za daleko
		http_global_buffer[http_header_position - 4] = 0;
		new http_method[32], http_status_str[32];
		new parse_result = parse(http_global_buffer, http_method, charsmax(http_method), http_status_str, charsmax(http_status_str));
		
		if(parse_result != 2 || !(equal(http_method, "HTTP/1.1") || equal(http_method, "HTTP/1.0")) || !equal(http_status_str, "200"))
		{
			http_on_failure_close_finalize(MALFORMED_HEADER);
			return;
		}
	}
	
	// Gdyby procedura obs�ugi postanowi�a stworzy� nowe po��czenie...
	new saved_http_global_buffer_pos = http_global_buffer_position;
	
	// Bufor wymaga zwolnienia
	if(must_free_buffer)
		http_global_buffer_position = 0;
		
	// Zamkni�cie po��czenia
	if(is_message_end)
		http_connection_close_finalize();
		
	// Informujemy u�ytkownika 
	if(reason != (http_progress_reason:-1))
		http_on_progress_data(reason, http_header_position, saved_http_global_buffer_pos);
}
stock http_on_progress_data(http_progress_reason:reason, start_index, end_index)
{
	new call[128];
	formatex(call, charsmax(call), "%s_progress", http_call_on_finish);
	
	new data_length = end_index - start_index;
	
	callfunc_begin(call);
	callfunc_push_int(reason);
	callfunc_push_array(http_global_buffer[start_index], data_length, false);
	callfunc_push_int(data_length);
	callfunc_end();
}
