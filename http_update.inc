#if defined _http_update_included
  #endinput
#endif
#define _http_update_included

#pragma semicolon 1

#include "http"

/**
	Rozpoczyna aktualizacj� pliku.
	
	Pod adresem data_url spodziewa si� do pobrania pliku z danymi
	Pod adresem md5_url spodziewa si� do pobrania sumy kontrolnej md5 s�u��cej do sprawdzenia czy plik jest aktualny, czy nie
	Pod local_file_path znajduje si� aktualizowana �cie�ka pliku
	callback_prefix - prefix dla funkcji:
		- {callback_prefix}_finished - aktualizacja zosta�a zako�czona pomy�lnie - lokalny plik jest aktualny. Argument m�wi czy plik zosta� zaktualizowany (true), czy by� ju� prawid�owy (false)
		- {callback_prefix}_error - aktualizacja zosta�a zako�czona b��d�m
	
	{callback_prefix}finished(updated)
	{callback_prefix}_error(http_update_error_reason:reason, http_error_reason:additional_reason)
*/

#define HTTP_UPDATE_URL_MAX_LENGTH 512
new http_update_data_url[HTTP_UPDATE_URL_MAX_LENGTH], http_update_md5_url[HTTP_UPDATE_URL_MAX_LENGTH];

#define HTTP_UPDATE_LOCAL_FILE_PATH_MAX_LENGTH 256
new http_update_local_file_path[HTTP_UPDATE_LOCAL_FILE_PATH_MAX_LENGTH], http_update_tmp_file_path[HTTP_UPDATE_LOCAL_FILE_PATH_MAX_LENGTH + 8];

#define HTTP_UPDATE_CALLBACK_PREFIX_MAX_LENGTH 128
new http_update_callback_prefix[HTTP_UPDATE_CALLBACK_PREFIX_MAX_LENGTH];

new http_update_current_md5[34];
new http_update_file_handle = 0;

enum http_update_error_reason { GET_MD5_FAILED, GET_DATA_FAILED, DATA_OPEN_FAILED, DATA_FILE_INVALID };

stock http_update_in_progress()
{
	return http_update_file_handle > 0;
}
stock http_update_begin(const data_url[], const md5_url[], const local_file_path[], const callback_prefix[])
{
	if(http_update_in_progress())
	{
		set_fail_state("http_update_in_progress()");
		return;
	}
	
	if(strlen(data_url) > charsmax(http_update_data_url))
	{
		set_fail_state("strlen(data_url) > charsmax(http_update_data_url)");
		return;
	}
	copy(http_update_data_url, charsmax(http_update_data_url), data_url);
	
	if(strlen(md5_url) > charsmax(http_update_md5_url))
	{
		set_fail_state("strlen(md5_url) > charsmax(http_update_md5_url)");
		return;
	}
	copy(http_update_md5_url, charsmax(http_update_md5_url), md5_url);
	
	if(strlen(local_file_path) > charsmax(http_update_local_file_path))
	{
		set_fail_state("strlen(local_file_path) > charsmax(http_update_local_file_path)");
		return;
	}
	copy(http_update_local_file_path, charsmax(http_update_local_file_path), local_file_path);
	formatex(http_update_tmp_file_path, charsmax(http_update_tmp_file_path), "%s.tmp", http_update_local_file_path);
	
	if(strlen(callback_prefix) > charsmax(http_update_callback_prefix))
	{
		set_fail_state("strlen(callback_prefix) > charsmax(http_update_callback_prefix)");
		return;
	}
	copy(http_update_callback_prefix, charsmax(http_update_callback_prefix), callback_prefix);
	
	http_update_current_md5[0] = 0;
	http_update_get_md5_begin();
}

stock http_update_on_error(http_update_error_reason:reason, http_error_reason:additional_reason)
{
	new call[HTTP_UPDATE_CALLBACK_PREFIX_MAX_LENGTH + 32];
	formatex(call, charsmax(call), "%s_error", http_update_callback_prefix);
	
	callfunc_begin(call);
	callfunc_push_int(reason);
	callfunc_push_int(additional_reason);
	callfunc_end();	
}
stock http_update_on_finished(updated)
{
	new call[HTTP_UPDATE_CALLBACK_PREFIX_MAX_LENGTH + 32];
	formatex(call, charsmax(call), "%s_finished", http_update_callback_prefix);
	
	callfunc_begin(call);
	callfunc_push_int(updated);
	callfunc_end();	
}

// ------------------- MD5 -------------------

stock http_update_get_md5_begin()
{
	// Czy u�ytkownik poda� w og�le sum� MD5?
	if(http_update_md5_url[0] == 0)
	{
		http_update_data_begin();
		return;
	}
	
	http_begin(http_update_md5_url, "http_update_get_md5");
}

public http_update_get_md5_progress(http_progress_reason:reason, const data[], data_length)
{
	// Spodziewamy si� bardzo kr�tkich danych w jednej transakcji
	if(reason != TRANSACTION_END)
	{
		http_kill();
		
		http_update_on_error(GET_MD5_FAILED, (http_error_reason:-1));
		return;
		
	}
	if(data_length != 32)
	{	
		http_update_on_error(GET_MD5_FAILED, (http_error_reason:-1));
		return;
	}
	
	// Sprawdzamy, czy otrzymana suma MD5 zawiera wy��cznie dozwolone znaki
	new all_md5_chars_valid = true;
	for(new i = 0; i < 32; i++)
	{
		if(!((data[i] >= '0' && data[i] <= '9') || (data[i] >= 'a' && data[i] <= 'f')))
		{
			all_md5_chars_valid = false;
			break;
		}
	}
	
	if(!all_md5_chars_valid)
	{
		http_update_on_error(GET_MD5_FAILED, (http_error_reason:-1));
		return;
	}
	
	copy(http_update_current_md5, data_length, data);
	
	// Na tym etapie w http_update_current_md5 znajduje si� suma MD5 pliku
	// Sprawdzamy, czy plik istnieje i czy posiada prawid�ow� sum�, kt�ra oznacza brak konieczno�ci aktualizacji
	
	// Plik nie istnieje - aktualizujemy
	if(!file_exists(http_update_local_file_path))
	{
		http_update_data_begin();
		return;
	}
	
	new md5_buffer[34];
	md5_file(http_update_local_file_path, md5_buffer);
	
	// Plik istnieje, suma niezgodna
	if(!equali(http_update_current_md5, md5_buffer))
	{
		http_update_data_begin();
		return;
	}
	
	// Plik istnieje, suma zgodna - koniec ;)
	http_update_on_finished(false);
}
public http_update_get_md5_error(http_error_reason:reason)
{
	http_update_on_error(GET_MD5_FAILED, reason);
}

// ------------------- DATA -------------------
stock http_update_data_begin()
{
	http_update_file_handle = fopen(http_update_tmp_file_path, "wb");
	if(!http_update_file_handle)
	{
		http_update_on_error(DATA_OPEN_FAILED, (http_error_reason:-1));
		return;
	}
	
	http_begin(http_update_data_url, "http_update_data");
}

public http_update_data_progress(http_progress_reason:reason, const data[], data_length)
{
	// Prze�aduj dane z socketa do pliku
	fwrite_blocks(http_update_file_handle, data, data_length, 1);
	
	if(reason == TRANSACTION_END)
	{
		// Zamknij plik i zwolnij uchwyt
		fclose(http_update_file_handle);
		http_update_file_handle = 0;
		
		// Wykonaj sprawdzeine pliku
		new downloaded_file_valid = true;
		
		if(downloaded_file_valid && file_size(http_update_tmp_file_path) <= 0)
			downloaded_file_valid = false;
		
		// Je�li istnieje suma MD5 - sprawd�, czy pobrany plik z
		if(downloaded_file_valid && http_update_current_md5[0] != 0)
		{
			new downloaded_md5[34];
			md5_file(http_update_tmp_file_path, downloaded_md5);
			if(!equali(downloaded_md5, http_update_current_md5))
				downloaded_file_valid = false;
		}
		
		if(downloaded_file_valid)
		{
			// Zmie� plik tymczasowy na docelowy
			rename_file(http_update_tmp_file_path, http_update_local_file_path, 1);
			http_update_on_finished(true);
		}
		else
		{
			// Usu� plik tymczasowy
			delete_file(http_update_tmp_file_path);
			http_update_on_error(DATA_FILE_INVALID, (http_error_reason:-1));
		}
	}
}
public http_update_data_error(http_error_reason:reason)
{
	http_update_on_error(GET_DATA_FAILED, reason);
}
